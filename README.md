## Name
Tic Tac Toe game

## Description
A Tic Tac Toe game to play in command line

## Badges
Ruby
Tic Tac Toe
Terminal

## Installation
Command line based code

## Usage
ruby TicTacToe.rb

## Roadmap
v1.0 basic code while learning ruby

## Authors and acknowledgment
Joris Puechlong

## License
Open Source

## Project status
V1.0 basic game ready
