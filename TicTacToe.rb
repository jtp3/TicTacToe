# Create TicTacToe Board
#Test update commit from web

class TicTacToe
    
    def initialize()
        # Create the game board and start with player 1
        puts "Welcome to this TicTacToe game!"
        puts "To play, write the line and then column of your move separated by a space. Value must be between 1 and 3"
        @TTT = Array.new(3) { Array.new(3) { '' } }
        @CurrentPlayer = 1

        # Option for the future to chose an AI player
    end

    # Game Starts here
    def play
        # As long as game is not finished (won or board full) keep playing
        loop do
            puts "what's your move player? #{@CurrentPlayer}"
            # Get the move from the player and make it into a proper integer / board position
            move = gets.chomp.split(' ')
            row = move[0].to_i - 1
            col = move[1].to_i - 1
            # If this is a legal move (case not taken)
            if IsLegalMove(row,col)
                @TTT[row][col] = @CurrentPlayer
                DisplayBoard()
                # Check if game is complete (won or board full)
                if IsGameComplete()
                    # I end my loop do
                    break
                end
                # I switch user and go back in the loop
                if @CurrentPlayer == 1
                    @CurrentPlayer = 2
                else
                    @CurrentPlayer = 1
                end
            end
        end
    end

        
    def IsLegalMove(row,col)
        if row >=0 && row <3 && col >=0 && col <3
            puts "move within the board"
            # puts "Current cell has value #{@TTT[row][col]}"
            # puts "Current board #{@TTT}"
            if @TTT[row][col] == ''
                puts("move allowed")
                return true
            else
                puts("cell already used")
                return false
            end
        else
            puts "This is not a correct move, you try to move outside of the board!"
            return false
        end
    end

    def IsGameComplete()
        @Full = true
        @Won = false
        # Check if board is full (if any cell is empty, then board is not full)
        3.times do |i|
            3.times do |j|
              if @TTT[i][j] == ''
                @Full = false
              end
            end
          end
        # check if 1 player won
        # Check if a line is completely filled by 1 player
        3.times do |i|
            if @TTT[i][0] == @TTT[i][1] && @TTT[i][1] == @TTT[i][2] && @TTT[i][0] != '' 
                @Won = true
            end
        end
        # Check if a column is completely filled by 1 player
        3.times do |j|
            if @TTT[0][j] == @TTT[1][j] && @TTT[1][j] == @TTT[2][j] && @TTT[0][j] != '' 
                @Won = true
            end
        end
        # Check if a diagonal is completely filled by 1 player
        if @TTT[0][0] == @TTT[1][1] && @TTT[1][1] == @TTT[2][2] && @TTT[0][0] != '' 
            @Won = true
        end
        # Check if a diagonal is completely filled by 1 player
        if @TTT[0][2] == @TTT[1][1] && @TTT[1][1] == @TTT[2][0] && @TTT[0][2] != '' 
            @Won = true
        end
        if @Won
            puts "Player #{@CurrentPlayer} won the game!"
        elsif @Full
            puts "Board is full, nobody won"
        end
        return @Won || @Full
    end
    
    def DisplayBoard()
        puts "Display board"
        3.times do |i|
            puts ' '
            3.times do |j|
              if @TTT[i][j] == ''
                print "' ' "
              else
                print "'#{@TTT[i][j]}' " 
              end
            end
        end
        puts
    end

end

# Code launching
Game = TicTacToe.new
Game.play()


